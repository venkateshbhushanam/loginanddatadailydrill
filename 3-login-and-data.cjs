const fs = require('fs')
const path = require('path')
const util = require('util')
const writeFile = util.promisify(fs.writeFile)
const unlinkFile = util.promisify(fs.unlink)
const readFile = util.promisify(fs.readFile);

const file1Path = path.join(__dirname, './file1')
const file2Path = path.join(__dirname, './file2')
const lipusmFilePath = path.join(__dirname, './lipsum.txt')
const newLipsumFilePath = path.join(__dirname, './newlipsum.txt')

/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/



/*Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)*/

function problem1() {

    let fileNames = [file1Path, file2Path]

    writeFile(fileNames[0], "file1data")
        .then(() => {
            console.log("created file1 ")
        })
        .catch(err => {
            console.log(err)
        })

    writeFile(fileNames[1], "file2data")
        .then(() => {
            console.log("created file2")
        })
        .catch(err => {
            console.log(err)
        })

    const delay = 2

    setTimeout(() => {
        unlinkFile(fileNames[0])
            .then(() => {
                console.log('deleted file1')
            })
            .catch((err) => {
                console.log(err)
            })

        unlinkFile(fileNames[1])
            .then(() => {
                console.log('deleted file2')
            })
            .catch((err) => {
                console.log(err)
            })
    }, delay * 1000)


}

/*Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file Delete the original file 
Using promise chaining*/


function problem2() {
    readFile(lipusmFilePath, 'utf-8')
        .then((lipsumData) => {
            return writeFile(newLipsumFilePath, lipsumData)
        })
        .then(() => {
            unlinkFile(lipusmFilePath)
                .then(() => {
                    console.log("deleted")
                })
        })
        .catch(err => {
            console.log(err)
        })


}


/*Use appropriate methods to 
A. login with value 3 and call getData once login is successful*/

function problem3A() {
    login("rajesh", 3)
        .then(() => {
            getData()

        })
        .catch((err) => {
            console.log(err)
        })

}




problem1()
problem2()
problem3A()

